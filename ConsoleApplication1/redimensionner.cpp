#include "include.h"
GLvoid redimensionner(int w,	int	h)	{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    /*glViewport(0,0, w,h);
	gluPerspective(80,w/(h*1.0),0.1,8);
	gluLookAt(0,0,4, 0,0,0, 0,1,0);*/
	gluOrtho2D(-10.0f, 10.0f, -10.0f, 10.0f);
	glViewport(0,0, w,h);
}
