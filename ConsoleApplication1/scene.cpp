#include "include.h"

void ligneVerticale() {
	
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor4f(0.20f,0.20f,0.2f, 1.0f);
	glVertex2f(0,0);
	glVertex2d(0,6);
	
	glEnd();

}

void ligneHorizontale() {
	glBegin(GL_LINES);
	glVertex2f(0,0);
	glVertex2f(6,0);
	
	glEnd();

}

void point() {
	glBegin(GL_TRIANGLE_FAN);

	glVertex2f(0.0f,0.0f);
	for ( int i=0; i<100; i++ ) {
		float t = 2* M_PI * (float)i/(float)100;
		glVertex2f(sin(t),cos(t));
	}
	glVertex2f(sin(0), cos(0));
	glEnd();
}

void image() {
	glBegin(GL_QUADS);
    // 1
	glColor4f(0.20f,0.20f,0.2f, 1.0f);
	glVertex2f( 1.0f,1.0f);
	glVertex2f(1.0f,-1.0f);
	glVertex2f(-1.0f,-1.0f);
	glVertex2f(-1.0f,1.0f);

	glEnd();


}