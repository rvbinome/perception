#ifndef hello
#define hello
#include <Windows.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>

extern GLfloat dx;
extern GLfloat dy;
extern GLint objet;
#define M_PI 3.14159265358979323846

#include "scene.h"
#include "redimensionner.h"
#include "affichage.h"
#endif