// TP1.cpp�: d�finit le point d'entr�e pour l'application console.
//

//	En-t�tes	pour	OpenGL	et	GLUT
#include "include.h"



GLfloat pasX = 0.1f;
GLfloat pasY = 0.1f;
bool imgDroite, imgGauche;

//	D�finition	de	la	fonction	g�rant	les	interruptions	clavier
GLvoid clavier(unsigned	char	touche,	int x,	int y)	{
	switch(touche)	{
	case	'z'	:	//	Augmentation du parallaxe verticale
		dy = dy + pasY;
		break;
	case	's'	:	//	Diminution du parallaxe verticale
		dy = dy - pasY;
		break;
	case	'q'	:	//	Augmentation du parallaxe horizontale
		dx = dx - pasX;
		break;
    case	'd'	:	//	Diminution du parallaxe horizontale
		dx = dx + pasX;
		break;
	case	'o'	: //Changement d'objet
		objet = (GLint)(objet+1)%4;
		break;
	case	'w'	: //Affichage image gauche
		imgGauche = !imgGauche;
		break;
	case	'x'	: //Affichage image droite
		imgDroite = !imgDroite;
		break;
	case	'f' : 
		std::cout<<"Le pas X actuel est de "<<pasX<<". Veuillez rentrer le nouveau"<<std::endl;
		std::cin>>pasX;
		break;
	case	'g' : 
		std::cout<<"Le pas Y actuel est de "<<pasY<<". Veuillez rentrer le nouveau"<<std::endl;
		std::cin>>pasY;
		break;
	case 'e' : // On quitte l'application
		exit(EXIT_SUCCESS);
	case 27	:
		exit(0);
		break;
	}
	std::cout<<"D�calage"<<dx<<" "<<dy<<std::endl;
	//	Demande	a	GLUT	de	reafficher	la	scene
	glutPostRedisplay();
}


int main(int argc, char* argv[] )
{
	plouf = 5;
	dx=0.0f; // Pas besoin de type car type d�j� donn� dans affichage.cpp. Nous ne comprenons pas r�ellement mais cel� a � voir avec le fait de l'avoir mis en extern dans include
	dy=0.0f;
	objet=0;
	//	Ini)alisa)on	de	GLUT
	glutInit(&argc,	argv);
	//	Choix	du	mode	d'affichage	(ici	RVB)
	glutInitDisplayMode(GLUT_RGB | GLUT_STEREO | GLUT_DOUBLE);
	//	Posi)on	ini)ale	de	la	fen�tre	GLUT
	glutInitWindowPosition(0,0);
	//	Taille	ini)ale	de	la	fen�tre	GLUT
	glutInitWindowSize(1700,1080);
	//	Cr�a)on	de	la	fen�tre	GLUT
	glutCreateWindow("Premier Exemple");
	glutFullScreen();
	//	D�fini)on	de	la	couleur	d'effacement
	//	du	framebuffer	OpenGL
	glClearColor(0.0f,	0.0f,	0.0f,	1.0f);
	glEnable(GL_STEREO);



	//	D�finition	des	fonctions	de	callbacks
	glutDisplayFunc(affichage);
	glutKeyboardFunc(clavier);
	glutReshapeFunc(redimensionner);

	//	Lancement	de	la	boucle	infinie	GLUT
	glutMainLoop();
	return 0;
}

